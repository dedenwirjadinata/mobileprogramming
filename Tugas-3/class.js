class Animal {
    constructor(name) {
        this.ayam = name ;
        this.bebek = 4 ;
        this.jepang = false;
} get name() {
    return this.ayam;
} get legs() {
    return this.bebek;
} get cold_blooded() {
    return this.jepang;
} set name(x) {
    this.ayam = x;
} set legs(y) {
    this.bebek = y;
} set cold_blooded(z) {
    this.jepang = false;
}
}
class Ape extends Animal {
    constructor(name) {
        super(name)
        this.ayam = name;
        this.bebek = 2;
    }
    yell() { return console.log("Auooo"); }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
        this.ayam = name;
    }
    jump() { return console.log("hop hop"); }
}
    
    

let sheep = new Animal("shaun");
console.log(sheep.ayam) // "shaun"
console.log(sheep.bebek) // 4
console.log(sheep.jepang) // false
console.log("");

let sungokong = new Ape("kera sakti")
console.log(sungokong.ayam); //"kera sakti"
console.log(sungokong.bebek); //2
console.log(sungokong.jepang) // false
sungokong.yell() // "Auooo"
console.log("");

let kodok = new Frog("buduk")
console.log(kodok.ayam); //buduk
console.log(kodok.bebek); //4
console.log(kodok.jepang) // false
kodok.jump() // "hop hop"
console.log("");

//02
class Clock {
    constructor({ template }) {
        this.template = template;
    }

    render() {
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
}
var clock = new Clock({ template: 'h:m:s' });
clock.start();